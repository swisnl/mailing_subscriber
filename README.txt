-- SUMMARY --
The mailing subscriber module provides a block with a form where user can put their email to subscribe to a mailist. 
This module also provides a form in the block configuration where you can select one of the implementations.

The module requires one of the implementation modules to work. 

-- INSTALLATION -- 
 - Install as usual, see http://drupal.org/node/1897420 for further information.

-- USAGE -- 
 - Install the module as described above
 - Install one or more of the available implementations, at least one is needed
 - Create a new block
 - Configure this block following the stept
 - Save the block
 - An email field with a subscribe button is shown on the block
 - Insert a valid email and click on subscribe
 - You are now subscribed to a mailing list

-- AVAILABLE IMPLEMENTATIONS -- 
 - Mailchimp - http://drupal.org/project/mailing_subscriber_mailchimp