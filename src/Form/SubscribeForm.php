<?php

namespace Drupal\mailing_subscriber\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mailing_subscriber\Exception\MailingSubscriberException;
use Drupal\mailing_subscriber\SubscriberInterface;

class SubscribeForm extends FormBase {

  protected $subscriber;

  public function __construct(SubscriberInterface $subscriber) {
    $this->subscriber = $subscriber;
  }

  public function getFormId() {
    return 'mailing_subscriber';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#cache'] = FALSE;
    $form['#token'] = FALSE;
    $form['#process'][] = [$this, 'processForm'];

    $wrapper_id = Html::getUniqueId('mailing-subscriber-form');
    $form['container'] = [
      '#type' => 'container',
      '#id' => $wrapper_id,
    ];

    if ($form_state->isSubmitted()) {
      if ($form_state->get('subscription_success')) {
        $form['container'] = [
          '#type' => 'inline_template',
          '#template' => '<p><em>{{message}}</em></p><script>window.dataLayer = window.dataLayer || []; dataLayer.push({{datalayer|raw}});</script>',
          '#context' => [
            'message' => $this->t('Thank you for your subscription!'),
            'datalayer' => json_encode(
              [
                'event' => 'newsletter_subscribe',
              ]
            ),
          ],
        ];

      }
      elseif ($form_state->get('subscription_error')) {
        $form['container']['#markup'] = '<p><em>' . $form_state->get(
            'subscription_error'
          ) . '</em></p>';
      }
      else {
        $form['container']['#markup'] = '<p><em>' . $this->t(
            "We couldn't process your subscription at the moment, please try again later."
          ) . '</em></p>';
      }
    }
    else {
      $form['container']['email'] = [
        '#type' => 'email',
        '#title' => $this->t('Email address'),
        '#title_display' => 'invisible',
        '#placeholder' => $this->t('Your email address...'),
        '#size' => 50,
        '#required' => TRUE,
      ];

      $submit = [
        '#type' => 'submit',
        '#name' => 'subscription_mail_submit',
        '#value' => $this->t('Sign up'),
        '#id' => Html::getUniqueId('custom-mailing-subscribe-form-submit'),
        '#ajax' => [
          'callback' => [$this,'submitFormAjax'],
          'wrapper' => $wrapper_id,
          'event' => 'click',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t("Signing up…"),
          ],
        ],
      ];

      $form['container']['submit_container'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['form-actions']],
        'subscription_mail_submit' => $submit,
      ];
    }

    return $form;
  }

  public function processForm(&$form, FormStateInterface $form_state) {
    $form['form_build_id']['#access'] = FALSE;
    $form['form_token']['#access'] = FALSE;
    $form_state->disableCache();
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');

    try {
      $this->subscriber->subscribe($email);
      $form_state->set('subscription_success', TRUE);
    } catch (MailingSubscriberException $e) {
      $form_state->set('subscription_success', FALSE);
      $form_state->set('subscription_error', $e->getMessage());
    }

    $form_state->setRebuild(TRUE);
  }

  public function submitFormAjax(array &$form, FormStateInterface $form_state) {
    return $form['container'];
  }
}
