<?php

namespace Drupal\mailing_subscriber\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Subscriber annotation.
 *
 * @Annotation
 */
class Subscriber extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the action plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
