<?php

namespace Drupal\mailing_subscriber\Plugin\mailing_subscriber\Subscriber;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\mailing_subscriber\SubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

abstract class SubscriberBase extends PluginBase implements SubscriberInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * @var LoggerInterface
   */
  protected $logger;

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    LoggerInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
  }


  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.mailing_subscriber')
    );
  }

  public function defaultConfiguration() {
    return [];
  }

  public function getConfiguration() {
    return $this->configuration;
  }

  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }


  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

}
