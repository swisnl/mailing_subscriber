<?php

namespace Drupal\mailing_subscriber\Plugin\mailing_subscriber\Subscriber;

use Drupal\Core\Annotation\Translation;
use Drupal\mailing_subscriber\Annotation\Subscriber;

/**
 * Defines a fallback plugin for missing subscriber plugins.
 *
 * @Subscriber(
 *   id = "broken",
 *   label = @Translation("Broken"),
 *   admin_label = @Translation("Broken/Missing"),
 *   category = @Translation("Subscriber"),
 * )
 */
final class Broken extends SubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('Broken type');
  }

  public function subscribe($email, array $extra_data = []) {
  }

}
