<?php

namespace Drupal\mailing_subscriber\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mailing_subscriber\Form\SubscribeForm;
use Drupal\mailing_subscriber\SubscriberManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *   id = "mailing_subscriber_form",
 *   admin_label = @Translation("Mailing subscriber form")
 * )
 */
class SubscribeFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * @var \Drupal\mailing_subscriber\SubscriberManager
   */
  protected $subscriberManager;


  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    FormBuilderInterface $form_builder,
    SubscriberManager $subscriberManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->subscriberManager = $subscriberManager;
  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('subscriber.manager')
    );
  }

  public function defaultConfiguration() {
    return [
        'subscriber' => NULL,
        'subscriber_settings' => [],
      ] + parent::defaultConfiguration();
  }

  public function blockForm($form, FormStateInterface $form_state) {
    // This method receives a sub form state instead of the full form state.
    // There is an ongoing discussion around this which could result in the
    // passed form state going back to a full form state. In order to prevent
    // future breakage because of a core update we'll just check which type of
    // FormStateInterface we've been passed and act accordingly.
    // @See https://www.drupal.org/node/2798261
    if ($form_state instanceof SubformStateInterface) {
      $form_state = $form_state->getCompleteFormState();
    }

    $form['subscriber'] = [
      '#type' => 'select',
      '#title' => $this->t('Mail Subscribers'),
      '#options' => $this->getSubscriberOptions(),
      '#default_value' => $this->configuration['subscriber'],
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this,'blockFormSelectionSelectSubmitAjax'],
        'event' => 'change',
        'wrapper' => 'subscriber_settings',
        'trigger_as' => ['name' => 'subscriber_select'],
        'progress' => [
          'type' => 'throbber',
          'message' => t('Getting configuration...'),
        ],
      ],
    ];

    $form['subscriber_select'] = [
      '#type' => 'submit',
      '#name' => 'subscriber_select',
      '#value' => t('Select subscriber'),
      '#limit_validation_errors' => [
        ['settings', 'subscriber'],
      ],
      '#submit' => [
        [$this, 'blockFormSelectionSelectSubmit']
      ],
      '#ajax' => [
        'callback' => [$this, 'blockFormSelectionSelectSubmitAjax'],
        'wrapper' => 'subscriber_settings',
      ],
      '#attributes' => ['class' => ['js-hide']],
    ];

    $form['subscriber_settings'] = [
      '#type' => 'container',
      '#id' => 'subscriber_settings',
    ];

    $subscriber = $form_state->getValue(['settings', 'subscriber'], $this->configuration['subscriber']);
    $subscriber_settings = $form_state->getValue(['settings', 'subscriber_settings'], $this->configuration['subscriber_settings']);

    $subform_state = SubformState::createForSubform($form['subscriber_settings'], $form, $form_state);

    $form['subscriber_settings'] = $this->subscriberManager
      ->createInstance($subscriber, $subscriber_settings)
      ->buildConfigurationForm($form['subscriber_settings'], $subform_state);

    return $form;
  }

  public function blockFormSelectionSelectSubmit($form, FormStateInterface $form_state) {
    // Clear subscriber settings when changing the subscriber.
    $form_state->setValue(['settings', 'subscriber_settings'], []);
    // Also discard the subscriber settings that were posted alongside the
    // subscriber change request.
    $user_input = $form_state->getUserInput();
    unset($user_input['settings']['subscriber_settings']);
    $form_state->setUserInput($user_input);

    $form_state->setRebuild();
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function blockFormSelectionSelectSubmitAjax(array &$form, FormStateInterface $form_state) {
    return $form['settings']['subscriber_settings'];

  }

  public function blockValidate($form, FormStateInterface $form_state) {
    // Note we always pass an empty settings array; the settings in
    // form state are not validated yet; the settings in $this->>configuration
    // might not be applicable to the selected subscriber.
    $subscriber = $form_state->getValue('subscriber');
    $subscriber_plugin = $this->subscriberManager->createInstance($subscriber);

    $subform_state = SubformState::createForSubform($form['subscriber_settings'], $form, $form_state);

    $subscriber_plugin->validateConfigurationForm($form['subscriber_settings'], $subform_state);

    $form_state->setValue('subscriber_settings', $subform_state->getValues());
  }

  public function blockSubmit($form, FormStateInterface $form_state) {
    // BlockForm::submitForm() is calling this method with a sub form state
    // while passing the complete form array. This results in nested
    // SubFormState instances from working correctly. To fix this, we
    // always use the complete form state.
    if ($form_state instanceof SubformStateInterface) {
      $form_state = $form_state->getCompleteFormState();
    }

    // Note we always pass an empty settings array; the settings in
    // form state are not validated yet; the settings in $this->>configuration
    // might not be applicable to the selected subscriber.
    $subscriber = $form_state->getValue(['settings', 'subscriber']);
    $subscriber_plugin = $this->subscriberManager->createInstance($subscriber);

    $subform_state = SubformState::createForSubform($form['settings']['subscriber_settings'], $form, $form_state);

    $subscriber_plugin->submitConfigurationForm($form['settings']['subscriber_settings'], $subform_state);

    $this->configuration['subscriber'] = $form_state->getValue(['settings', 'subscriber']);
    $this->configuration['subscriber_settings'] = $subscriber_plugin->getConfiguration();
  }


  public function build() {
    $subscriber = $this->subscriberManager->createInstance(
      $this->configuration['subscriber'],
      $this->configuration['subscriber_settings']
    );
    $form_object = new SubscribeForm($subscriber);
    $form_state = new FormState();

    return $this->formBuilder->buildForm($form_object, $form_state);
  }

  public function getSubscriberOptions() {
    $options = [];
    $definitions = $this->subscriberManager->getVisibleDefinitions();
    foreach ($definitions as $subscriber) {
      $options[$subscriber['id']] = $subscriber['label'];
    }

    return $options;
  }

}
