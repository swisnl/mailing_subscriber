<?php

namespace Drupal\mailing_subscriber;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

interface SubscriberInterface extends ConfigurableInterface, PluginInspectionInterface, PluginFormInterface {

  /**
   * Get the label.
   *
   * @return string
   *   The label.
   */
  public function getLabel();

  /**
   * Subscribe the given email address.
   *
   * @param string $email
   * @param array $extra_data
   */
  public function subscribe($email);

}
